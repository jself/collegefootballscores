08/28/1991|Georgia Tech|22|Penn State|34
08/29/1991|Brigham Young|28|Florida St|44
08/31/1991|Weber St|31|Air Force|48
08/31/1991|Miami FL|31|Arkansas|3
08/31/1991|Georgia Southern|17|Auburn|32
08/31/1991|Eastern Michigan|6|Bowling Green St|17
08/31/1991|Arkansas St|24|Colorado St|38
08/31/1991|Western Carolina|0|Georgia|48
08/31/1991|Louisiana Tech|3|Houston|73
08/31/1991|East Carolina|31|Illinois|38
08/31/1991|Eastern Kentucky|14|Louisville|24
08/31/1991|Louisiana-Monroe|21|Louisiana-Lafayette|10
08/31/1991|Ball St|7|Miami OH|15
08/31/1991|Fullerton St|3|Mississippi St|47
08/31/1991|Central Michigan|17|Ohio|17
08/31/1991|Sacramento St|43|Pacific|40
08/31/1991|Boston College|13|Rutgers|20
08/31/1991|Delta St|7|Southern Miss|25
08/31/1991|Mississippi|22|Tulane|3
08/31/1991|Southwest Missouri St|13|Tulsa|34
08/31/1991|New Mexico|19|UTEP|35
08/31/1991|Utah St|7|Utah|12
08/31/1991|James Madison|12|Virginia Tech|41
08/31/1991|Pittsburgh|34|West Virginia|3
08/31/1991|Kent|10|Western Michigan|13
08/31/1991|Hawaii|32|Wyoming|17
09/02/1991|Memphis|24|Southern Cal|10
09/05/1991|Tennessee|28|Louisville|11
09/07/1991|Temple|3|Alabama|41
09/07/1991|SMU|6|Arkansas|17
09/07/1991|Northwestern St|28|Arkansas St|3
09/07/1991|UTEP|7|Baylor|27
09/07/1991|Michigan|35|Boston College|13
09/07/1991|Pacific|24|California|86
09/07/1991|Louisiana-Lafayette|24|Central Michigan|27
09/07/1991|Appalachian St|0|Clemson|34
09/07/1991|Wyoming|13|Colorado|30
09/07/1991|Air Force|31|Colorado St|26
09/07/1991|San Jose St|21|Florida|59
09/07/1991|Tulane|11|Florida St|38
09/07/1991|Northern Illinois|7|Fresno St|55
09/07/1991|LSU|10|Georgia|31
09/07/1991|Hawaii|10|Iowa|53
09/07/1991|Eastern Illinois|13|Iowa St|42
09/07/1991|Indiana St|25|Kansas St|26
09/07/1991|Miami OH|20|Kentucky|23
09/07/1991|Virginia|6|Maryland|17
09/07/1991|Mississippi|10|Memphis|0
09/07/1991|Texas|6|Mississippi St|13
09/07/1991|Ball St|33|Navy|10
09/07/1991|Utah St|28|Nebraska|59
09/07/1991|UNLV|8|Nevada|50
09/07/1991|Virginia Tech|0|North Carolina St|7
09/07/1991|Indiana|27|Notre Dame|49
09/07/1991|Arizona|14|Ohio State|38
09/07/1991|Washington St|14|Oregon|40
09/07/1991|Utah|22|Oregon St|10
09/07/1991|Cincinnati|0|Penn State|81
09/07/1991|Southern Miss|14|Pittsburgh|35
09/07/1991|Eastern Michigan|3|Purdue|49
09/07/1991|Duke|24|South Carolina|24
09/07/1991|Washington|42|Stanford|7
09/07/1991|Vanderbilt|10|Syracuse|37
09/07/1991|New Mexico|7|TCU|60
09/07/1991|Fullerton St|7|Texas Tech|41
09/07/1991|Kansas|30|Toledo|7
09/07/1991|Oklahoma St|7|Tulsa|13
09/07/1991|Brigham Young|23|UCLA|27
09/07/1991|Bowling Green St|17|West Virginia|24
09/07/1991|Akron|12|Western Michigan|35
09/08/1991|Long Beach St|13|San Diego St|49
09/12/1991|Houston|10|Miami FL|40
09/14/1991|Illinois St|25|Akron|3
09/14/1991|Stanford|23|Arizona|28
09/14/1991|Colgate|22|Army|51
09/14/1991|Mississippi|13|Auburn|23
09/14/1991|Long Beach St|14|Boise St|48
09/14/1991|Georgia Tech|30|Boston College|14
09/14/1991|Purdue|18|California|42
09/14/1991|Baylor|16|Colorado|14
09/14/1991|Rutgers|22|Duke|42
09/14/1991|Memphis|13|East Carolina|20
09/14/1991|Alabama|0|Florida|35
09/14/1991|Western Michigan|0|Florida St|58
09/14/1991|New Mexico|13|Hawaii|35
09/14/1991|Iowa|29|Iowa St|10
09/14/1991|Tulsa|17|Kansas|23
09/14/1991|Montana|11|Louisiana Tech|21
09/14/1991|Syracuse|31|Maryland|17
09/14/1991|Eastern Michigan|3|Miami OH|29
09/14/1991|Notre Dame|14|Michigan|24
09/14/1991|Central Michigan|20|Michigan St|3
09/14/1991|San Jose St|20|Minnesota|26
09/14/1991|Tulane|0|Mississippi St|48
09/14/1991|Illinois|19|Missouri|23
09/14/1991|Colorado St|14|Nebraska|71
09/14/1991|Cincinnati|16|North Carolina|51
09/14/1991|Kent|0|North Carolina St|47
09/14/1991|Arkansas St|21|Northern Illinois|22
09/14/1991|Rice|36|Northwestern|7
09/14/1991|Tennessee Tech|14|Ohio|35
09/14/1991|Louisville|15|Ohio State|23
09/14/1991|North Texas|2|Oklahoma|40
09/14/1991|Arizona St|30|Oklahoma St|3
09/14/1991|Temple|7|Pittsburgh|26
09/14/1991|Pacific|34|San Diego St|55
09/14/1991|Penn State|10|Southern Cal|21
09/14/1991|Ball St|16|TCU|22
09/14/1991|UCLA|16|Tennessee|30
09/14/1991|LSU|7|Texas A&M|45
09/14/1991|Oregon|28|Texas Tech|13
09/14/1991|SMU|11|Vanderbilt|14
09/14/1991|Navy|10|Virginia|17
09/14/1991|Fresno St|34|Washington St|30
09/14/1991|South Carolina|16|West Virginia|21
09/14/1991|Louisiana-Lafayette|15|Wyoming|28
09/19/1991|Virginia|21|Georgia Tech|24
09/21/1991|San Diego St|20|Air Force|21
09/21/1991|Georgia|0|Alabama|10
09/21/1991|California|23|Arizona|21
09/21/1991|Louisiana-Lafayette|7|Arkansas|9
09/21/1991|North Carolina|20|Army|12
09/21/1991|Kent|27|Ball St|28
09/21/1991|Missouri|21|Baylor|47
09/21/1991|Cincinnati|16|Bowling Green St|20
09/21/1991|East Carolina|47|Central Florida|25
09/21/1991|Temple|7|Clemson|37
09/21/1991|Minnesota|0|Colorado|58
09/21/1991|Akron|29|Central Michigan|31
09/21/1991|Colgate|14|Duke|42
09/21/1991|Louisiana Tech|17|Eastern Michigan|14
09/21/1991|Northridge St|10|Fullerton St|17
09/21/1991|Pacific|21|Hawaii|30
09/21/1991|Houston|10|Illinois|51
09/21/1991|Kentucky|10|Indiana|13
09/21/1991|New Mexico St|14|Kansas|54
09/21/1991|Northern Illinois|17|Kansas St|34
09/21/1991|Vanderbilt|14|LSU|16
09/21/1991|San Jose St|32|Long Beach St|20
09/21/1991|West Virginia|37|Maryland|7
09/21/1991|Arkansas St|21|Memphis|31
09/21/1991|Ohio|14|Mississippi|38
09/21/1991|William & Mary|26|Navy|21
09/21/1991|Washington|36|Nebraska|21
09/21/1991|UNLV|23|New Mexico|22
09/21/1991|Michigan St|10|Notre Dame|49
09/21/1991|Washington St|19|Ohio State|33
09/21/1991|Utah St|21|Oklahoma|55
09/21/1991|TCU|24|Oklahoma St|21
09/21/1991|Fresno St|24|Oregon St|20
09/21/1991|Brigham Young|7|Penn State|33
09/21/1991|Northwestern|18|Rutgers|22
09/21/1991|Virginia Tech|21|South Carolina|28
09/21/1991|Arizona St|32|Southern Cal|25
09/21/1991|Colorado St|7|Southern Miss|39
09/21/1991|Florida|21|Syracuse|38
09/21/1991|Mississippi St|24|Tennessee|26
09/21/1991|Auburn|14|Texas|10
09/21/1991|Rice|28|Tulane|19
09/21/1991|Texas A&M|34|Tulsa|35
09/21/1991|Northwestern St|0|UTEP|14
09/21/1991|Oregon|17|Utah|24
09/21/1991|North Carolina St|30|Wake Forest|3
09/21/1991|Toledo|23|Western Michigan|13
09/21/1991|Iowa St|6|Wisconsin|7
09/21/1991|Texas Tech|17|Wyoming|22
09/26/1991|UCLA|37|San Diego St|12
09/28/1991|Long Beach St|21|Arizona|45
09/28/1991|Louisiana Tech|42|Arkansas St|10
09/28/1991|Northern Arizona|14|Akron|49
09/28/1991|Nebraska|18|Arizona St|9
09/28/1991|Harvard|20|Army|21
09/28/1991|Air Force|7|Brigham Young|21
09/28/1991|Miami OH|22|Cincinnati|9
09/28/1991|Georgia Tech|7|Clemson|9
09/28/1991|Hawaii|16|Colorado St|28
09/28/1991|South Carolina|20|East Carolina|31
09/28/1991|Mississippi St|7|Florida|29
09/28/1991|Fullerton St|14|Georgia|27
09/28/1991|Ball St|14|Indiana St|10
09/28/1991|Northern Illinois|7|Iowa|58
09/28/1991|Kent|6|Kentucky|24
09/28/1991|Southern Miss|14|Louisville|28
09/28/1991|Florida St|51|Michigan|31
09/28/1991|Rutgers|14|Michigan St|7
09/28/1991|Pittsburgh|14|Minnesota|13
09/28/1991|Arkansas|17|Mississippi|24
09/28/1991|Indiana|27|Missouri|27
09/28/1991|Bowling Green St|22|Navy|19
09/28/1991|New Mexico St|10|New Mexico|17
09/28/1991|North Carolina|7|North Carolina St|24
09/28/1991|Wake Forest|14|Northwestern|41
09/28/1991|Virginia Tech|17|Oklahoma|27
09/28/1991|Southern Cal|30|Oregon|14
09/28/1991|Boston College|21|Penn State|28
09/28/1991|Notre Dame|45|Purdue|20
09/28/1991|Iowa St|28|Rice|27
09/28/1991|Baylor|45|SMU|7
09/28/1991|Colorado|21|Stanford|28
09/28/1991|Howard|0|Temple|40
09/28/1991|Auburn|21|Tennessee|30
09/28/1991|Louisiana-Lafayette|7|Texas A&M|34
09/28/1991|TCU|30|Texas Tech|16
09/28/1991|Central Michigan|16|Toledo|16
09/28/1991|Syracuse|24|Tulane|0
09/28/1991|Miami FL|34|Tulsa|10
09/28/1991|Washington St|40|UNLV|13
09/28/1991|Alabama|48|Vanderbilt|17
09/28/1991|Duke|3|Virginia|34
09/28/1991|Kansas St|3|Washington|56
09/28/1991|Ohio|9|Western Michigan|35
09/28/1991|Eastern Michigan|6|Wisconsin|21
09/28/1991|San Jose St|23|Utah St|7
09/28/1991|UTEP|28|Wyoming|28
10/04/1991|Utah St|12|Brigham Young|38
10/05/1991|Akron|20|East Carolina|56
10/05/1991|Arkansas|22|TCU|21
10/05/1991|Appalachian St|17|Wake Forest|3
10/05/1991|Army|12|Rutgers|14
10/05/1991|Arizona|0|Washington|54
10/05/1991|Baylor|38|Houston|21
10/05/1991|California|27|UCLA|24
10/05/1991|Central Florida|31|Arkansas St|20
10/05/1991|Central Michigan|10|Bowling Green St|17
10/05/1991|Cincinnati|30|Louisville|7
10/05/1991|Clemson|12|Georgia|27
10/05/1991|Colorado St|23|UTEP|18
10/05/1991|East Tennessee St|7|South Carolina|55
10/05/1991|Eastern Michigan|21|Kent|20
10/05/1991|Florida|16|LSU|0
10/05/1991|Fullerton St|28|Pacific|56
10/05/1991|Georgia Tech|21|North Carolina St|28
10/05/1991|Kansas|19|Virginia|31
10/05/1991|Long Beach St|31|UNLV|19
10/05/1991|Louisiana Tech|37|Northern Illinois|3
10/05/1991|Maryland|20|Pittsburgh|24
10/05/1991|Memphis|21|Missouri|31
10/05/1991|Miami OH|27|Louisiana-Lafayette|14
10/05/1991|Michigan|43|Iowa|24
10/05/1991|Michigan St|0|Indiana|31
10/05/1991|Minnesota|3|Illinois|24
10/05/1991|Mississippi|35|Kentucky|14
10/05/1991|New Mexico|17|Fresno St|94
10/05/1991|New Mexico St|6|Oregon|29
10/05/1991|Notre Dame|42|Stanford|26
10/05/1991|Ohio|13|Toledo|17
10/05/1991|Oklahoma|29|Iowa St|8
10/05/1991|Oklahoma St|3|Miami FL|40
10/05/1991|Oregon St|7|Washington St|55
10/05/1991|Penn State|24|Temple|7
10/05/1991|Purdue|17|Northwestern|14
10/05/1991|Rice|7|Texas|28
10/05/1991|San Diego St|47|Hawaii|21
10/05/1991|SMU|31|Tulane|17
10/05/1991|Southern Miss|10|Auburn|9
10/05/1991|Syracuse|14|Florida St|46
10/05/1991|Tennessee-Chattanooga|7|Alabama|53
10/05/1991|Texas A&M|37|Texas Tech|14
10/05/1991|Utah|15|Arizona St|21
10/05/1991|Vanderbilt|13|Duke|17
10/05/1991|Virginia Tech|20|West Virginia|14
10/05/1991|Western Michigan|25|Ball St|16
10/05/1991|William & Mary|36|North Carolina|59
10/05/1991|Wisconsin|16|Ohio State|31
10/05/1991|Wyoming|28|Air Force|51
10/12/1991|Air Force|46|Navy|6
10/12/1991|Arizona|14|UCLA|54
10/12/1991|Arizona St|24|Oregon St|7
10/12/1991|Arkansas St|14|LSU|70
10/12/1991|Auburn|24|Vanderbilt|22
10/12/1991|Ball St|10|Eastern Michigan|8
10/12/1991|Bowling Green St|45|Ohio|14
10/12/1991|Cal Poly-San Luis Obispo|28|Pacific|63
10/12/1991|Cincinnati|38|Kent|19
10/12/1991|Citadel|20|Army|14
10/12/1991|Cornell|6|Stanford|56
10/12/1991|East Carolina|23|Syracuse|20
10/12/1991|Georgia|37|Mississippi|17
10/12/1991|Houston|17|Arkansas|29
10/12/1991|Iowa|10|Wisconsin|6
10/12/1991|Kansas|12|Kansas St|16
10/12/1991|Kentucky|6|Mississippi St|31
10/12/1991|Long Beach St|14|Fresno St|42
10/12/1991|Louisiana Tech|12|South Carolina|12
10/12/1991|Louisville|3|Boston College|33
10/12/1991|Maine|17|Rutgers|40
10/12/1991|Maryland|10|Georgia Tech|34
10/12/1991|Miami OH|10|Central Michigan|10
10/12/1991|Michigan|45|Michigan St|28
10/12/1991|Missouri|7|Colorado|55
10/12/1991|Nebraska|49|Oklahoma St|15
10/12/1991|UNLV|25|Fullerton St|3
10/12/1991|New Mexico|24|San Diego St|38
10/12/1991|Northwestern|6|Indiana|44
10/12/1991|Ohio State|7|Illinois|10
10/12/1991|Oklahoma|7|Texas|10
10/12/1991|Oregon|7|California|45
10/12/1991|Penn State|20|Miami FL|26
10/12/1991|Pittsburgh|7|Notre Dame|42
10/12/1991|Purdue|3|Minnesota|6
10/12/1991|Rice|20|Baylor|17
10/12/1991|San Jose St|39|New Mexico St|13
10/12/1991|Southern Cal|34|Washington St|27
10/12/1991|Southern Miss|12|Memphis|17
10/12/1991|Temple|9|West Virginia|10
10/12/1991|Tennessee|18|Florida|35
10/12/1991|Texas Tech|38|SMU|14
10/12/1991|UTEP|29|Brigham Young|31
10/12/1991|Toledo|0|Washington|48
10/12/1991|Tulane|0|Alabama|62
10/12/1991|Tulsa|34|Louisiana-Lafayette|20
10/12/1991|Utah|57|Wyoming|42
10/12/1991|Virginia|20|Clemson|20
10/12/1991|Virginia Tech|20|Florida St|33
10/12/1991|Wake Forest|10|North Carolina|24
10/12/1991|Western Michigan|22|Northern Illinois|10
10/12/1991|Youngstown St|24|Akron|38
10/19/1991|Arkansas St|23|Akron|28
10/19/1991|Army|37|Louisville|12
10/19/1991|Central Michigan|23|Kent|7
10/19/1991|Cincinnati|9|Virginia Tech|56
10/19/1991|Colorado|34|Oklahoma|17
10/19/1991|Colorado St|16|Utah|21
10/19/1991|Fresno St|42|New Mexico St|28
10/19/1991|Fullerton St|3|Utah St|26
10/19/1991|Georgia|25|Vanderbilt|27
10/19/1991|Georgia Tech|14|South Carolina|23
10/19/1991|Hawaii|18|Brigham Young|35
10/19/1991|Illinois|21|Iowa|24
10/19/1991|Indiana|16|Michigan|24
10/19/1991|Iowa St|0|Kansas|41
10/19/1991|Kansas St|31|Nebraska|38
10/19/1991|Long Beach St|0|Miami FL|55
10/19/1991|LSU|29|Kentucky|26
10/19/1991|Marshall|14|North Carolina St|15
10/19/1991|Maryland|23|Wake Forest|22
10/19/1991|Memphis|28|Mississippi St|23
10/19/1991|Middle Tennessee St|10|Florida St|39
10/19/1991|Minnesota|12|Michigan St|20
10/19/1991|Navy|14|Temple|21
10/19/1991|North Carolina|9|Virginia|14
10/19/1991|Northern Illinois|10|Florida|41
10/19/1991|Northwestern|3|Ohio State|34
10/19/1991|Notre Dame|28|Air Force|15
10/19/1991|Ohio|0|Miami OH|34
10/19/1991|Oklahoma St|7|Missouri|41
10/19/1991|Pacific|47|San Jose St|64
10/19/1991|Rutgers|17|Penn State|37
10/19/1991|San Diego St|28|UTEP|21
10/19/1991|SMU|20|Houston|49
10/19/1991|Stanford|24|Southern Cal|21
10/19/1991|Syracuse|31|Pittsburgh|27
10/19/1991|Tennessee|19|Alabama|24
10/19/1991|Texas|13|Arkansas|14
10/19/1991|Texas A&M|34|Baylor|12
10/19/1991|TCU|39|Rice|28
10/19/1991|Toledo|21|Bowling Green St|24
10/19/1991|Tulane|14|Southern Miss|47
10/19/1991|UCLA|44|Oregon St|7
10/19/1991|Washington|24|California|17
10/19/1991|Washington St|17|Arizona St|3
10/19/1991|West Virginia|31|Boston College|24
10/19/1991|Western Michigan|24|Eastern Michigan|42
10/19/1991|Wisconsin|7|Purdue|28
10/19/1991|Wyoming|39|New Mexico|19
10/25/1991|Michigan|52|Minnesota|6
10/26/1991|Ball St|3|Central Michigan|10
10/26/1991|Boston College|28|Army|17
10/26/1991|Bowling Green St|23|Western Michigan|10
10/26/1991|Brigham Young|41|New Mexico|23
10/26/1991|Colorado|10|Kansas St|0
10/26/1991|Colorado St|28|Wyoming|35
10/26/1991|Delaware|29|Navy|25
10/26/1991|Duke|17|Maryland|13
10/26/1991|Florida St|27|LSU|16
10/26/1991|Houston|18|Texas A&M|27
10/26/1991|Illinois|11|Northwestern|17
10/26/1991|Indiana|28|Wisconsin|20
10/26/1991|Iowa|31|Purdue|21
10/26/1991|Kansas|3|Oklahoma|41
10/26/1991|Kent|40|Ohio|45
10/26/1991|Kentucky|27|Georgia|49
10/26/1991|Louisiana Tech|14|Louisiana-Lafayette|14
10/26/1991|Louisville|13|Virginia Tech|41
10/26/1991|Miami FL|36|Arizona|9
10/26/1991|Miami OH|7|Toledo|24
10/26/1991|Michigan St|17|Ohio State|27
10/26/1991|Mississippi St|24|Auburn|17
10/26/1991|Missouri|6|Nebraska|63
10/26/1991|UNLV|22|Fresno St|48
10/26/1991|New Mexico St|20|Pacific|27
10/26/1991|North Carolina|14|Georgia Tech|35
10/26/1991|North Carolina St|19|Clemson|29
10/26/1991|Northern Illinois|7|Akron|17
10/26/1991|Oklahoma St|6|Iowa St|6
10/26/1991|Oregon|7|Washington|29
10/26/1991|Oregon St|10|Stanford|40
10/26/1991|Pittsburgh|23|East Carolina|24
10/26/1991|Rice|20|Texas Tech|40
10/26/1991|San Diego St|24|Utah|21
10/26/1991|San Jose St|20|California|41
10/26/1991|Southern Cal|20|Notre Dame|24
10/26/1991|Southern Miss|7|Cincinnati|17
10/26/1991|Southwest Missouri St|37|Arkansas St|20
10/26/1991|Syracuse|21|Rutgers|7
10/26/1991|Texas|34|SMU|0
10/26/1991|TCU|9|Baylor|26
10/26/1991|UTEP|13|Air Force|20
10/26/1991|Tulsa|33|Memphis|28
10/26/1991|UCLA|21|Arizona St|16
10/26/1991|Utah St|6|Long Beach St|7
10/26/1991|Vanderbilt|30|Mississippi|27
10/26/1991|Wake Forest|7|Virginia|48
10/26/1991|West Virginia|6|Penn State|51
10/31/1991|Brigham Young|40|Colorado St|17
11/02/1991|Air Force|32|New Mexico|34
11/02/1991|Arizona St|16|Washington|44
11/02/1991|Baylor|9|Arkansas|5
11/02/1991|Bowling Green St|17|Miami OH|7
11/02/1991|Central Michigan|14|Eastern Michigan|14
11/02/1991|Cincinnati|17|Kentucky|20
11/02/1991|Florida|31|Auburn|10
11/02/1991|Florida St|40|Louisville|15
11/02/1991|Fresno St|19|Utah St|20
11/02/1991|Georgia Tech|17|Duke|6
11/02/1991|Iowa|16|Ohio State|9
11/02/1991|Iowa St|23|Missouri|22
11/02/1991|Kansas|31|Oklahoma St|0
11/02/1991|Kansas St|7|Oklahoma|28
11/02/1991|LSU|25|Mississippi|22
11/02/1991|Louisiana-Lafayette|13|Northern Illinois|12
11/02/1991|Louisiana-Monroe|10|Louisiana Tech|35
11/02/1991|Maryland|0|North Carolina|24
11/02/1991|Memphis|24|Tennessee|52
11/02/1991|Minnesota|8|Indiana|34
11/02/1991|Mississippi St|7|Alabama|13
11/02/1991|Navy|0|Notre Dame|38
11/02/1991|Nebraska|19|Colorado|19
11/02/1991|UNLV|12|San Jose St|55
11/02/1991|New Mexico St|35|Fullerton St|12
11/02/1991|North Carolina St|38|South Carolina|21
11/02/1991|Northwestern|16|Michigan St|13
11/02/1991|Ohio|6|Ball St|10
11/02/1991|Oregon St|21|Arizona|45
11/02/1991|Pacific|51|Long Beach St|24
11/02/1991|Pittsburgh|12|Boston College|38
11/02/1991|Purdue|0|Michigan|42
11/02/1991|Rutgers|3|West Virginia|28
11/02/1991|Southern Cal|30|California|52
11/02/1991|SMU|10|TCU|18
11/02/1991|Southern Miss|10|Tulsa|13
11/02/1991|Stanford|33|Oregon|13
11/02/1991|Temple|6|Syracuse|27
11/02/1991|Texas A&M|38|Rice|21
11/02/1991|Texas Tech|15|Texas|23
11/02/1991|Toledo|13|Kent|14
11/02/1991|Tulane|28|East Carolina|38
11/02/1991|Utah|26|Hawaii|52
11/02/1991|Vanderbilt|41|Army|10
11/02/1991|Virginia Military Institute|0|Virginia|42
11/02/1991|Wake Forest|10|Clemson|28
11/02/1991|Washington St|3|UCLA|44
11/02/1991|Wisconsin|6|Illinois|22
11/02/1991|Wyoming|22|San Diego St|24
11/09/1991|Akron|24|Virginia Tech|42
11/09/1991|Alabama|20|LSU|17
11/09/1991|Arizona|27|Washington St|40
11/09/1991|Arkansas|21|Texas Tech|38
11/09/1991|Army|0|Air Force|25
11/09/1991|Ball St|9|Toledo|3
11/09/1991|Boston College|33|Temple|13
11/09/1991|California|27|Oregon St|14
11/09/1991|Clemson|21|North Carolina|6
11/09/1991|Colorado|16|Oklahoma St|12
11/09/1991|Colorado St|32|San Diego St|42
11/09/1991|Duke|14|Wake Forest|31
11/09/1991|East Carolina|48|Southern Miss|20
11/09/1991|Eastern Michigan|13|Ohio|10
11/09/1991|Florida|45|Georgia|13
11/09/1991|Fresno St|59|Pacific|14
11/09/1991|Fullerton St|7|San Jose St|35
11/09/1991|Furman|17|Georgia Tech|19
11/09/1991|Illinois|41|Purdue|14
11/09/1991|Indiana|21|Iowa|38
11/09/1991|Kansas St|37|Iowa St|7
11/09/1991|Kent|7|Bowling Green St|35
11/09/1991|Kentucky|7|Vanderbilt|17
11/09/1991|Long Beach St|24|New Mexico St|28
11/09/1991|Louisiana-Lafayette|7|Auburn|50
11/09/1991|Louisville|7|Memphis|35
11/09/1991|Miami OH|23|Western Michigan|24
11/09/1991|Michigan St|20|Wisconsin|7
11/09/1991|Middle Tennessee St|10|Cincinnati|30
11/09/1991|Navy|7|Tulane|34
11/09/1991|Nebraska|59|Kansas|23
11/09/1991|New Mexico|7|Utah|30
11/09/1991|Northwestern|14|Michigan|59
11/09/1991|Ohio State|35|Minnesota|6
11/09/1991|Oklahoma|56|Missouri|16
11/09/1991|Oregon|21|Arizona St|24
11/09/1991|Penn State|47|Maryland|7
11/09/1991|Rice|31|SMU|10
11/09/1991|Rutgers|17|Pittsburgh|22
11/09/1991|South Carolina|10|Florida St|38
11/09/1991|Southern Illinois|16|Louisiana Tech|48
11/09/1991|Tennessee|35|Notre Dame|34
11/09/1991|Texas|14|Houston|23
11/09/1991|Texas A&M|44|TCU|7
11/09/1991|UTEP|41|Hawaii|24
11/09/1991|Troy St|17|Arkansas St|20
11/09/1991|UCLA|10|Stanford|27
11/09/1991|Utah St|27|UNLV|14
11/09/1991|Virginia|42|North Carolina St|10
11/09/1991|Washington|14|Southern Cal|3
11/09/1991|West Virginia|3|Miami FL|27
11/09/1991|Wyoming|31|Brigham Young|56
11/16/1991|Akron|0|Army|19
11/16/1991|Alabama|10|Memphis|7
11/16/1991|Arizona St|6|California|25
11/16/1991|Arkansas|3|Texas A&M|13
11/16/1991|Auburn|27|Georgia|37
11/16/1991|Boston College|16|Syracuse|38
11/16/1991|Bowling Green St|14|Ball St|13
11/16/1991|Brigham Young|52|San Diego St|52
11/16/1991|East Carolina|24|Virginia Tech|17
11/16/1991|Fullerton St|7|Fresno St|38
11/16/1991|Hawaii|35|San Jose St|35
11/16/1991|Houston|41|Rice|21
11/16/1991|Illinois St|24|Northern Illinois|27
11/16/1991|Indiana|16|Ohio State|20
11/16/1991|Iowa|24|Northwestern|10
11/16/1991|Iowa St|13|Nebraska|38
11/16/1991|Kansas|24|Colorado|30
11/16/1991|Kentucky|26|Florida|35
11/16/1991|Louisiana-Lafayette|17|Arkansas St|13
11/16/1991|Louisville|0|Tulsa|40
11/16/1991|Maryland|7|Clemson|40
11/16/1991|Miami FL|17|Florida St|16
11/16/1991|Miami OH|20|Kent|9
11/16/1991|Michigan|20|Illinois|0
11/16/1991|Michigan St|17|Purdue|27
11/16/1991|Mississippi|25|Tennessee|36
11/16/1991|Mississippi St|28|LSU|19
11/16/1991|Missouri|0|Kansas St|32
11/16/1991|New Mexico|38|Colorado St|36
11/16/1991|New Mexico St|28|UNLV|38
11/16/1991|North Carolina St|32|Duke|31
11/16/1991|Notre Dame|13|Penn State|35
11/16/1991|Oklahoma St|6|Oklahoma|21
11/16/1991|Oregon|7|UCLA|16
11/16/1991|Pacific|14|Utah St|21
11/16/1991|South Carolina|17|North Carolina|21
11/16/1991|Southern Cal|14|Arizona|31
11/16/1991|Southern Miss|14|Louisiana Tech|30
11/16/1991|Stanford|49|Washington St|14
11/16/1991|Temple|0|Rutgers|41
11/16/1991|TCU|0|Texas|32
11/16/1991|Texas Tech|31|Baylor|24
11/16/1991|Toledo|21|Eastern Michigan|14
11/16/1991|Utah|10|UTEP|9
11/16/1991|Wake Forest|3|Georgia Tech|27
11/16/1991|Washington|58|Oregon St|6
11/16/1991|Western Michigan|17|Central Michigan|27
11/16/1991|Wisconsin|19|Minnesota|16
11/23/1991|Air Force|24|Hawaii|20
11/23/1991|Akron|37|Temple|32
11/23/1991|Arizona|14|Arizona St|37
11/23/1991|Baylor|21|Texas|11
11/23/1991|California|21|Stanford|38
11/23/1991|Clemson|41|South Carolina|24
11/23/1991|Colorado|17|Iowa St|14
11/23/1991|Duke|14|North Carolina|47
11/23/1991|East Carolina|30|Cincinnati|19
11/23/1991|Houston|45|TCU|49
11/23/1991|Illinois|24|Michigan St|27
11/23/1991|Kansas St|36|Oklahoma St|26
11/23/1991|Long Beach St|36|Fullerton St|37
11/23/1991|LSU|39|Tulane|20
11/23/1991|Louisiana Tech|21|UTEP|17
11/23/1991|Maryland|17|North Carolina St|20
11/23/1991|Miami FL|19|Boston College|14
11/23/1991|Minnesota|8|Iowa|23
11/23/1991|Mississippi|9|Mississippi St|24
11/23/1991|Missouri|29|Kansas|53
11/23/1991|Northern Illinois|21|Toledo|42
11/23/1991|Northwestern|14|Wisconsin|32
11/23/1991|Ohio|13|Tulsa|45
11/23/1991|Ohio State|3|Michigan|31
11/23/1991|Oregon St|14|Oregon|3
11/23/1991|Pacific|44|UNLV|23
11/23/1991|Purdue|22|Indiana|24
11/23/1991|Rice|0|Arkansas|20
11/23/1991|San Jose St|28|Fresno St|31
11/23/1991|SMU|6|Texas A&M|65
11/23/1991|Tennessee|16|Kentucky|7
11/23/1991|UCLA|24|Southern Cal|21
11/23/1991|Utah|17|Brigham Young|48
11/23/1991|Utah St|46|New Mexico St|21
11/23/1991|Virginia Tech|0|Virginia|38
11/23/1991|Wake Forest|52|Navy|24
11/23/1991|Washington St|21|Washington|56
11/23/1991|West Virginia|10|Syracuse|16
11/28/1991|Penn State|32|Pittsburgh|20
11/28/1991|Texas|14|Texas A&M|31
11/29/1991|Oklahoma|14|Nebraska|19
11/30/1991|Alabama|13|Auburn|6
11/30/1991|Clemson|33|Duke|21
11/30/1991|Florida St|9|Florida|14
11/30/1991|Georgia|18|Georgia Tech|15
11/30/1991|Notre Dame|48|Hawaii|42
11/30/1991|San Diego St|12|Miami FL|39
11/30/1991|Texas Tech|52|Houston|46
11/30/1991|Tulsa|31|SMU|26
11/30/1991|Vanderbilt|0|Tennessee|45
12/07/1991|Army|3|Navy|24
12/14/1991|Bowling Green St|28|Fresno St|21
12/25/1991|Georgia Tech|18|Stanford|17
12/28/1991|Alabama|30|Colorado|25
12/29/1991|Air Force|38|Mississippi St|15
12/29/1991|Arkansas|15|Georgia|24
12/29/1991|Oklahoma|48|Virginia|14
12/30/1991|Brigham Young|13|Iowa|13
12/30/1991|San Diego St|17|Tulsa|28
12/31/1991|Baylor|0|Indiana|24
12/31/1991|Illinois|3|UCLA|6
01/01/1992|California|37|Clemson|13
01/01/1992|East Carolina|37|North Carolina St|34
01/01/1992|Florida|28|Notre Dame|39
01/01/1992|Florida St|10|Texas A&M|2
01/01/1992|Michigan|14|Washington|34
01/01/1992|Nebraska|0|Miami FL|22
01/01/1992|Ohio State|17|Syracuse|24
01/01/1992|Penn State|42|Tennessee|17
